import * as appRoot from'app-root-path';
import * as mkdirp from 'mkdirp';


const createToRoot = (dirName :string) :void => {
    mkdirp.sync(appRoot.path + dirName)
}

export default createToRoot;


