"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appRoot = require("app-root-path");
const mkdirp = require("mkdirp");
const createToRoot = (dirName) => {
    mkdirp.sync(appRoot.path + dirName);
};
exports.default = createToRoot;
//# sourceMappingURL=index.js.map